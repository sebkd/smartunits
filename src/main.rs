//P1L4

enum TypeUnit {
    // Перечисление типов умной розетки
    Eur,
    Us,
}

struct SmartACOutlet {
    //  Структура данных умной розетки
    current_max: usize,
    voltage: usize,
    current: usize,
    description: String,
    mode: bool,
}

enum MeasureUnit {
    // Перечисление типов единиц измерения для умного термометра
    C,
    F,
}

struct SmartThermometer {
    max_value: isize,
    min_value: isize,
    current_value: isize,
    measure_unit: MeasureUnit,
    mode: bool,
}

impl SmartACOutlet {
    // Создать новую умную розетку
    fn new(desc: &str, type_smart: &TypeUnit) -> Self {
        let voltage = match *type_smart {
            TypeUnit::Eur => 220,
            TypeUnit::Us => 110,
        };
        SmartACOutlet {
            current_max: 16,
            voltage,
            description: desc.to_string(),
            current: 0,
            mode: false,
        }
    }

    fn get_description(&self) -> String {
        // Отдать описание умной розетки если устройство включено
        match self.mode {
            true => self.description.to_string(),
            false => "unit is off".to_string(),
        }
    }

    fn switch_mode(&mut self) -> String {
        // Переключить режим работы умной розетки
        match self.mode {
            true => {
                self.mode = false;
                "unit is off".to_string()
            }
            false => {
                self.mode = true;
                "unit is on".to_string()
            }
        }
    }

    fn set_current(&mut self, i: usize) {
        // установить ток через розетку
        match self.mode {
            true => {
                if i < self.current_max {
                    self.current = i
                }
            }
            false => {}
        }
    }
    fn get_current(&self) -> usize {
        // предоставить данные о текущей потребляемой мощности
        match self.mode {
            true => self.current * self.voltage,
            false => 0,
        }
    }
}

impl SmartThermometer {
    fn new(max: i32, min: i32) -> Self {
        // Создать новую сущность термометра
        SmartThermometer {
            max_value: max as isize,
            min_value: min as isize,
            current_value: 0,
            measure_unit: MeasureUnit::C,
            mode: false,
        }
    }

    fn switch_measure_unit(&mut self) -> String {
        // Изменить единицу измерения
        match self.mode {
            true => match self.measure_unit {
                MeasureUnit::C => {
                    self.measure_unit = MeasureUnit::F;
                    self.current_value = ((self.current_value * 9) / 5) + 32;
                    "switch mode to F".to_string()
                }
                MeasureUnit::F => {
                    self.measure_unit = MeasureUnit::C;
                    self.current_value = ((self.current_value - 32) * 5) / 9;
                    "switch mode to C".to_string()
                }
            },
            false => "device is off".to_string(),
        }
    }
    fn switch_mode(&mut self) -> String {
        // Переключить режим работы термометра
        match self.mode {
            true => {
                self.mode = false;
                "unit is off".to_string()
            }
            false => {
                self.mode = true;
                "unit is on".to_string()
            }
        }
    }
    fn set_temp(&mut self, t: isize) {
        // установить температуру
        match self.mode {
            true => {
                if t < self.max_value && t > self.min_value {
                    self.current_value = t;
                }
            }
            false => {}
        }
    }
    fn get_temp(&self) -> &isize {
        // предоставить данные о текущей температуре
        match self.mode {
            true => &self.current_value,
            false => &0,
        }
    }
}

fn main() {
    println!("Начало работы");
    let mut outlet = SmartACOutlet::new("main_room", &TypeUnit::Eur);
    assert_eq!(outlet.get_description(), "unit is off");
    println!("Описание розетки: {}", outlet.get_description());
    assert_eq!(outlet.get_current(), 0);
    println!("Выдаваемая мощность: {} Вт", outlet.get_current());
    outlet.switch_mode();
    assert_eq!(outlet.get_description(), "main_room");
    println!("Описание розетки: {}", outlet.get_description());
    assert_eq!(outlet.get_current(), 0);
    println!("Выдаваемая мощность: {} Вт", outlet.get_current());
    outlet.set_current(23);
    assert_eq!(outlet.get_current(), 0);
    println!("Выдаваемая мощность: {} Вт", outlet.get_current());
    outlet.set_current(12);
    assert_eq!(outlet.get_current(), 2640);
    println!("Выдаваемая мощность: {} Вт", outlet.get_current());

    let mut thermo = SmartThermometer::new(100, -100);
    // Негативный тест: установить температуру в mode off
    thermo.set_temp(72);
    assert_eq!(&0, thermo.get_temp());
    println!("Текущая температура {}", thermo.get_temp());
    // Позитивный тест: установить темперутуру в mode on
    thermo.switch_mode();
    thermo.set_temp(72);
    assert_eq!(&72, thermo.get_temp());
    println!("Текущая температура {}", thermo.get_temp());
    // Переключить единицу измерения
    thermo.switch_measure_unit();
    assert_eq!(&161, thermo.get_temp());
    println!("Текущая температура {}", thermo.get_temp());
}
